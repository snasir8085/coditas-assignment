package org.nasir.assignment.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Repo {
	
	@JsonProperty("name")
	private String repoName;

	@JsonProperty("url")
	@JsonAlias("http_url_to_repo")
	private String repoPath;
	
	@JsonProperty("created_at")
	private String repoCreatedDt;
	
	@JsonProperty("owner")
	@JsonAlias("namespace")
	private User owner;
	
	@JsonProperty("http_url_to_repo")
	@JsonAlias("clone_url")
	private String repoSrc;
	
	@JsonProperty("message")
	private String errorCode;
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the repoName
	 */
	public String getRepoName() {
		return repoName;
	}
	/**
	 * @param repoName the repoName to set
	 */
	public void setRepoName(String repoName) {
		this.repoName = repoName;
	}
	/**
	 * @return the repoPath
	 */
	public String getRepoPath() {
		return repoPath;
	}
	/**
	 * @param repoPath the repoPath to set
	 */
	public void setRepoPath(String repoPath) {
		this.repoPath = repoPath;
	}
	/**
	 * @return the repoCreatedDt
	 */
	public String getRepoCreatedDt() {
		return repoCreatedDt;
	}
	/**
	 * @param repoCreatedDt the repoCreatedDt to set
	 */
	public void setRepoCreatedDt(String repoCreatedDt) {
		
		this.repoCreatedDt=repoCreatedDt;
		 
	}
	/**
	 * @return the repoSrc
	 */
	public String getRepoSrc() {
		return repoSrc;
	}
	/**
	 * @param repoSrc the repoSrc to set
	 */
	public void setRepoSrc(String repoSrc) {
		if(!repoSrc.isEmpty()){
			if(repoSrc.contains("github")){
				this.repoSrc ="GitHub";
			}else{
				this.repoSrc ="GitLab";
			}
		}else{
			this.repoSrc = repoSrc;
		}
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}

	
	
}
