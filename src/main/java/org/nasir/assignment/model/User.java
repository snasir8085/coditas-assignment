package org.nasir.assignment.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;


public class User {
	
	
	
	
	@JsonProperty("login")
	@JsonAlias("path")
	private String userId;
	
	@JsonProperty("html_url")
	@JsonAlias("web_url")
	private String userName;
	

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the repoList
	 */

	
}
