package org.nasir.assignment.controller;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;


public class RestWsClient {


	public static String callGetForWebService(String restUrl, int seconds) {

		
		String output = null;
		int sec;
		if(seconds>60){
			sec = 60*1000;
		}else{
			sec = seconds*1000;
		}
		try {
			ClientConfig clientConfig = new DefaultClientConfig();
			clientConfig.getProperties().put(ClientConfig.PROPERTY_CONNECT_TIMEOUT, sec);
			clientConfig.getProperties().put(ClientConfig.PROPERTY_READ_TIMEOUT, sec);

			Client client = Client.create(clientConfig);

			WebResource webResource = client.resource(restUrl);

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				output = "{\"message\":\"INVALID\",\"errorMessage\":\" Failed : HTTP error code :  "+ response.getStatus() + "\"}";
			}
			output = response.getEntity(String.class);
		} catch (Exception e) {
			output = "{\"errorCode\":\"INVALID\",\"errorMessage\":\" " + e.getMessage() + "\"}";
		}

		return output;

	}
	
	/**
	 * 
	 * @param restUrl
	 * @return
	 */
	public String callGetForWebService(String restUrl) {
		return callGetForWebService(restUrl,  10);
		
	}
}
