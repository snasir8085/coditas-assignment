package org.nasir.assignment.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nasir.assignment.model.Repo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class UserController {

	private static final String ERROR = "error";
	private static final String MAINPAGE = "mainPage";
	private static final String USERDETAILPAGE = "userdetails";
	private static final String ERRORMSG = "Some Error Occur Please try again.";

	@RequestMapping(value = "/getUserDetails", method = RequestMethod.GET)
	public ModelAndView getUserDetails(@RequestParam("userId") String userId)

	{
		if (userId == null || userId.length() <= 0) {
			return new ModelAndView(MAINPAGE);
		}
		ModelAndView modelAndView = new ModelAndView(USERDETAILPAGE);
		Repo[] resultListarray = null;
		Repo[] resultListarray2 = null;
		List<Repo> repoList = new ArrayList<>();
		try {

			ObjectMapper objectMapperResp = new ObjectMapper()
					.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			objectMapperResp.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			String url = "https://api.github.com/users/" + userId + "/repos";
			RestWsClient wsclient = new RestWsClient();

			String jsonRespStr = wsclient.callGetForWebService(url);

			if (jsonRespStr.contains("message")) {
				modelAndView = new ModelAndView(MAINPAGE);
				modelAndView.addObject(ERROR, "User Not Found.");
			} else {
				resultListarray = objectMapperResp.readValue(jsonRespStr, Repo[].class);
				repoList.addAll(Arrays.asList(resultListarray));
			}

			if (resultListarray != null && resultListarray.length == 0) {

			}
			url = "https://gitlab.com/api/v4/users/" + userId + "/projects";
			jsonRespStr = wsclient.callGetForWebService(url);

			if (jsonRespStr.contains("message")) {
				if (repoList.isEmpty()) {
					modelAndView = new ModelAndView(MAINPAGE);
					modelAndView.addObject(ERROR, "User Not Found.");
					return modelAndView;
				} else {
					modelAndView = new ModelAndView(MAINPAGE);
					modelAndView.addObject(ERROR, "User Not Found.");
					return modelAndView;
				}
			} else {
				resultListarray2 = objectMapperResp.readValue(jsonRespStr, Repo[].class);
				repoList.addAll(Arrays.asList(resultListarray2));
			}

			modelAndView.addObject("userId", userId);
			modelAndView.addObject("userReportData", repoList);

			return modelAndView;

		} catch (Exception e) {
			modelAndView = new ModelAndView(MAINPAGE);
			modelAndView.addObject(ERROR, ERRORMSG);
		}

		return modelAndView;
	}

}
