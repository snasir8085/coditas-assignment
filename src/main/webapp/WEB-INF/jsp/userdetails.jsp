<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<body>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<style>
.margins {
	margin-bottom: 10%;
	margin-top: 10%;
	margin-right: 2%;
	margin-left: 2%;
}

body {
	padding-top: 40px;
	padding-bottom: 40px;
}

.form-signin {
	max-width: 600px;
	padding: 15px;
	margin: 0 auto;
	border: solid 1px grey;
}

.form-signin .form-signin-heading {
	margin-bottom: 10% !important;
	text-align: center;
}
</style>

</head>

<div class="container">
	<c:if test="${!empty(error)}">
		<form class="form-signin" action="" method="GET">
			<h2 class="form-signin-heading">
			<small>Web API to Search User's From GitHub and GitLab</small>
			</h2>
			<div class="form-group text-center margins">
				<label for="inputZipcode" class="form-label-text">Error in
					Request :<c:out value="${error}" />
				</label> <input type="hidden" name="userId" id="userId"
					class="form-control">
				<button class="btn btn-lg btn-primary" type="submit">Back</button>
			</div>
		</form>
	</c:if>
	<c:if test="${empty(error)}">
		<form class="form-signin" action="" method="GET">
			<h2 class="form-signin-heading">
				<small>Web API to Search User's From GitHub and GitLab</small>
			</h2>
			<div class="form-group margins">
				<table border="1">
					<tbody>
						<tr>
							<td style="width: 150px;">Repo Name</td>
							<td style="width: 150px;">Created Date</td>
							<td style="width: 150px;">Source</td>
							<td style="width: 150px;">Owner Id</td>
						</tr>
						<c:forEach items="${userReportData}" var="repo" >
						<tr>
							<td style="width: 150px;"><a href="${repo.repoPath}">${repo.repoName}</a></td>
							<td style="width: 150px;">${repo.repoCreatedDt}</td>
							<td style="width: 150px;">${repo.repoSrc}</td>
							<td style="width: 150px;"><a href="${repo.owner.userName}">${repo.owner.userId}</a></td>
							
						</tr>
						</c:forEach>
						
					</tbody>
				</table>
			</div>
			<div class="form-group text-center margins">
				<input type="hidden" name="userId" id="userId"
					class="form-control">
				<button class="btn btn-lg btn-primary" type="submit">Back</button>
			</div>
		</form>
	</c:if>
</div>
</body>

</html>

